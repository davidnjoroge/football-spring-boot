package com.shika.football.repository;

import java.util.Optional;

import com.shika.football.model.SeasonLeague;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonLeagueRepository extends JpaRepository<SeasonLeague, Long> {
    Optional<SeasonLeague> findBySeasonIdAndLeagueId(Long seasonId, Long leagueId);
}
