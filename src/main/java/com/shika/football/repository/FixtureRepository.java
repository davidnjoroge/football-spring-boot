package com.shika.football.repository;

import java.util.List;

import com.shika.football.model.Fixture;
import com.shika.football.model.SeasonLeague;
import com.shika.football.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FixtureRepository extends JpaRepository<Fixture, Long> {
    List<Fixture> findBySeasonLeague(SeasonLeague seasonLeague);

    Fixture findBySeasonLeagueAndHomeAndAway(SeasonLeague seasonLeague, Team home, Team away);

    List<Fixture>  findBySeasonLeagueAndMatchWeek(SeasonLeague seasonLeague, Long matchWeek);
}
