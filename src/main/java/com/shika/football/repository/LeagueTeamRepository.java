package com.shika.football.repository;

import java.util.List;
import java.util.Optional;

import com.shika.football.model.LeagueTeam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeagueTeamRepository extends JpaRepository<LeagueTeam, Long> {
    List<LeagueTeam> findByLeagueId(Long leagueId);


    Optional<LeagueTeam> findByTeamId(Long teamId);
}
