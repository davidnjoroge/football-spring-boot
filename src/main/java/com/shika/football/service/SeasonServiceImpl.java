package com.shika.football.service;

import com.shika.football.NotFoundException;
import com.shika.football.model.Season;
import com.shika.football.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SeasonServiceImpl implements SeasonService {

    @Autowired
    SeasonRepository seasonRepository;

    public Season findBySeasonId(Long seasonId) {
        return seasonRepository.findById(seasonId).orElseThrow(()-> new NotFoundException("season"));
    }
}
