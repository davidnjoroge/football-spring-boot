package com.shika.football.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.shika.football.model.LeagueTeam;
import com.shika.football.model.Team;
import com.shika.football.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    LeagueTeamService leagueTeamService;

    @Autowired
    TeamRepository teamRepository;

    public Optional<Team> findByName(String name) {
        return teamRepository.findByName(name);
    }

    public List<Team> findAll() {
        return teamRepository.findAll();
    }



    public List<Team> bulkSaveTeams(List<String> teamNames) {
        List<Team> teamList = new ArrayList<>();

        for (String teamName: teamNames) {
            Team foundTeam = teamRepository.findByName(teamName).orElse(null);

            if (foundTeam == null) {
                Team newTeam = new Team(teamName);
                teamRepository.save(newTeam);
                teamList.add(newTeam);
            } else {
                teamList.add(foundTeam);
            }
        }

        return teamList;
    }


    public Team getTeamById(Long teamId) {
        return teamRepository.findById(teamId).orElse(null);
    }

}
