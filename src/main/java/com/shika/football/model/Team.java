package com.shika.football.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Team implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.All.class)
    private Long id;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private String name;

    @JsonView(Views.All.class)
    private String location;

    @JsonView(Views.All.class)
    private LocalDate dateFounded;

    @JsonView(Views.All.class)
    private String manager;


    public Team(String name) {
        this.name = name;
    }

    public Team() {
    }

    //    @JsonView(Views.All.class)
    public List<Player> getTeamPlayers() {
        List<Player> players = new ArrayList<>();

        for (TeamPlayer teamPlayer: teamPlayers) {
            players.add(teamPlayer.getPlayer());
        }
        return players;
    }

    public void setTeamPlayers(List<TeamPlayer> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }

//    @JsonView(Views.LeagueDetailsView.class)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name= "team_id")
    private List<TeamPlayer> teamPlayers = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getDateFounded() {
        return dateFounded;
    }

    public void setDateFounded(LocalDate dateFounded) {
        this.dateFounded = dateFounded;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

}
