package com.shika.football.controller;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.shika.football.model.League;
import com.shika.football.model.Team;
import com.shika.football.model.Views;
import com.shika.football.repository.LeagueRepository;
import com.shika.football.request.LeagueRequest;
import com.shika.football.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/leagues")
public class LeagueController {

    @Autowired
    LeagueRepository leagueRepository;

    @Autowired
    LeagueService leagueService;

    @GetMapping("")
    @JsonView(Views.LeagueListView.class)
    public Iterable<League> getAllLeagues() {
        return leagueRepository.findAll ();
    }

    @GetMapping("/{leagueId}")
    @JsonView(Views.LeagueDetailsView.class)
    public League getByLeagueId(@PathVariable("leagueId") Long leagueId) {
        return leagueService.getById(leagueId);
    }

    @GetMapping("/{leagueId}/teams")
    @JsonView(Views.LeagueDetailsView.class)
    public List<Team> getTeamsByLeagueId(@PathVariable("leagueId") Long leagueId) {
        return leagueService.getTeamsByLeagueId(leagueId);
    }

    @PostMapping("/{leagueId}/teams")
    @JsonView(Views.LeagueDetailsView.class)
    public List<Team> saveTeamsToLeague(@PathVariable("leagueId") Long leagueId, @RequestBody() List<String> teams) {
        leagueService.saveLeagueTeams(leagueId, teams);
        return leagueService.getTeamsByLeagueId(leagueId);
    }

    @GetMapping("/custom")
    public LeagueRequest calculated() {
        return new LeagueRequest();
    }

}
