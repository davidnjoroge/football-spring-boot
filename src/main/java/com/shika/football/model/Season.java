package com.shika.football.model;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Season implements Serializable {
    @Id
    @JsonView(Views.All.class)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private LocalDate startYear;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private LocalDate endYear;

    @JsonView(Views.DetailsView.class)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name= "season_id")
    private List<SeasonLeague> seasonLeagues = new ArrayList<>();

    public List<League> getSeasonLeagues() {
        return seasonLeagues.stream().map(SeasonLeague::getLeague).collect(Collectors.toList());
    }

    public void setSeasonLeagues(List<SeasonLeague> seasonLeagues) {
        this.seasonLeagues = seasonLeagues;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartYear() {
        return startYear;
    }

    public void setStartYear(LocalDate startYear) {
        this.startYear = startYear;
    }

    public LocalDate getEndYear() {
        return endYear;
    }

    public void setEndYear(LocalDate endYear) {
        this.endYear = endYear;
    }
}
