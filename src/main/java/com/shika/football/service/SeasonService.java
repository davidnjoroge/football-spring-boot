package com.shika.football.service;

import com.shika.football.model.Season;

public interface SeasonService {

    Season findBySeasonId(Long seasonId);

}
