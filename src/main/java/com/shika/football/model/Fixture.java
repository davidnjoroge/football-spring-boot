package com.shika.football.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Fixture implements Serializable {

    @Id
    @JsonView(Views.All.class)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @JsonView(Views.All.class)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private Team home;

    @JsonView(Views.All.class)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private Team away;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private LocalDate fixtureDate;

    @JsonView(Views.All.class)
    private String fixtureTime;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private Long matchWeek;

    @JsonView(Views.All.class)
    private Long homeScore;

    @JsonView(Views.All.class)
    private Long awayScore;

    @JsonView(Views.All.class)
    private String results;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private SeasonLeague seasonLeague;


    /**
     * default constructor
     */
    public Fixture() {
        // default constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Team getHome() {
        return home;
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public Team getAway() {
        return away;
    }

    public void setAway(Team away) {
        this.away = away;
    }

    public String getFixtureTime() {
        return fixtureTime;
    }

    public void setFixtureTime(String fixtureTime) {
        this.fixtureTime = fixtureTime;
    }

    public Long getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Long homeScore) {
        this.homeScore = homeScore;
    }

    public Long getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Long awayScore) {
        this.awayScore = awayScore;
    }

    public LocalDate getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(LocalDate fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public Long getMatchWeek() {
        return matchWeek;
    }

    public void setMatchWeek(Long matchWeek) {
        this.matchWeek = matchWeek;
    }

    public SeasonLeague getLeagueSeason() {
        return seasonLeague;
    }

    public void setLeagueSeason(SeasonLeague leagueSeason) {
        this.seasonLeague = leagueSeason;
    }
}
