package com.shika.football.service;

import com.shika.football.model.League;
import com.shika.football.model.Season;
import com.shika.football.model.SeasonLeague;
import org.springframework.stereotype.Service;

@Service
public interface SeasonLeagueService {
    SeasonLeague findBySeasonAndLeague(Season season, League league);

    SeasonLeague findBySeasonLeagueId(Long seasonLeagueId);
}
