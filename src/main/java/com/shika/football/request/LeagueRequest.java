package com.shika.football.request;

public class LeagueRequest {
    private Long someNumberCalculated;

    public LeagueRequest(Long someNumberCalculated) {
        this.someNumberCalculated = someNumberCalculated;
    }

    public LeagueRequest() {
        this.someNumberCalculated = 2L+8L;
    }

    public Long getSomeNumberCalculated() {
        return someNumberCalculated;
    }

    public void setSomeNumberCalculated(Long someNumberCalculated) {
        this.someNumberCalculated = someNumberCalculated;
    }
}
