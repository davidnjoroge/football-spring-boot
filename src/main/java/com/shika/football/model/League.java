package com.shika.football.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class League implements Serializable {

    @Id
    @JsonView({Views.All.class, Views.SeasonDetailView.class})
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @JsonView(Views.All.class)
    @Column(nullable = false)
    private String name;

    @JsonView(Views.All.class)
    private Long maxTeams;

    @JsonView(Views.All.class)
    private String description;

//    @JsonView(Views.All.class)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name= "league_id")
    private List<LeagueTeam> leagueTeams = new ArrayList<>();

    public List<Team> getLeagueTeams() {
        List<Team> teams = new ArrayList<>();

        for (LeagueTeam leagueTeam : leagueTeams) {
            teams.add(leagueTeam.getTeam());
        }
        return teams;
    }

    public void setLeagueTeams(List<LeagueTeam> leagueTeams) {
        this.leagueTeams = leagueTeams;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMaxTeams() {
        return maxTeams;
    }

    public void setMaxTeams(Long maxTeams) {
        this.maxTeams = maxTeams;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
