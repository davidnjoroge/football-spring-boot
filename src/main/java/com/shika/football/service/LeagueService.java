package com.shika.football.service;

import java.util.List;

import com.shika.football.model.League;
import com.shika.football.model.Team;

public interface LeagueService {

    League getById(Long id);

    List<Team> getTeamsByLeagueId(Long id);

    List<Team> saveLeagueTeams(Long leagueId, List<String> teams);


}
