package com.shika.football.service;

import java.util.List;
import java.util.Optional;

import com.shika.football.model.LeagueTeam;
import com.shika.football.model.Team;

public interface LeagueTeamService {
    public List<LeagueTeam> findAllByLeagueId(Long leagueId);

    public LeagueTeam save(LeagueTeam leagueTeam);

    public Optional<LeagueTeam> findByTeam(Team team);
}
