package com.shika.football.service;

import java.util.List;

import com.shika.football.NotFoundException;
import com.shika.football.model.Fixture;
import com.shika.football.model.SeasonLeague;
import com.shika.football.model.Team;
import com.shika.football.repository.FixtureRepository;
import com.shika.football.repository.SeasonLeagueRepository;
import com.shika.football.request.FixtureRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FixtureServiceImpl implements FixtureService {
    @Autowired
    FixtureRepository fixtureRepository;

    @Autowired
    SeasonLeagueRepository seasonLeagueRepository;

    @Autowired
    TeamService teamService;

    @Autowired
    FixtureService fixtureService;


    public List<Fixture> findBySeasonAndLeague(Long seasonId, Long leagueId) {
        SeasonLeague seasonLeague = seasonLeagueRepository.findBySeasonIdAndLeagueId(seasonId, leagueId).orElse(null);
        return fixtureRepository.findBySeasonLeague(seasonLeague);
    }

    public Fixture save(Fixture fixture) {
        return fixtureRepository.save(fixture);
    }

    public Fixture createFixture(SeasonLeague seasonLeague, FixtureRequest fixtureRequest){
        Fixture fixture = new Fixture();
        fixture.setHome(teamService.findByName(fixtureRequest.getHome()).orElse(null));
        fixture.setAway(teamService.findByName(fixtureRequest.getAway()).orElse(null));
        fixture.setFixtureDate(fixtureRequest.getFixtureDate());
        fixture.setFixtureTime(fixtureRequest.getFixtureTime());
        fixture.setMatchWeek(fixtureRequest.getMatchWeek());
        fixture.setHomeScore(fixtureRequest.getHomeScore());
        fixture.setAwayScore(fixtureRequest.getAwayScore());
        fixture.setLeagueSeason(seasonLeague);
        if (fixture.getAway() == null) {
            return null;
        }
        if (fixture.getHome() == null) {
            return null;
        }
        return fixture;
    }

    public Fixture getOrCreateFixture(SeasonLeague seasonLeague, FixtureRequest fixtureRequest) {
        Fixture newFixture = createFixture(seasonLeague, fixtureRequest);
        Fixture foundFixture = fixtureService.findBySeasonLeagueAndHomeAndAway(seasonLeague, newFixture.getHome(), newFixture.getAway());

        if (foundFixture != null) {
            return foundFixture;
        }
        return fixtureService.save(newFixture);
    }

    public Fixture findBySeasonLeagueAndHomeAndAway(SeasonLeague seasonLeague, Team home, Team away) {
        return fixtureRepository.findBySeasonLeagueAndHomeAndAway(seasonLeague, home, away);
    }

    public List<Fixture> getWeekFixturesForSeasonLeague(Long seasonLeagueId, Long week) {
        SeasonLeague seasonLeague = seasonLeagueRepository.findById(seasonLeagueId).orElseThrow(()-> new NotFoundException("seasonLeague not found"));
        return fixtureRepository.findBySeasonLeagueAndMatchWeek(seasonLeague, week);
    }

}
