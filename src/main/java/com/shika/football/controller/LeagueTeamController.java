package com.shika.football.controller;

import com.shika.football.model.LeagueTeam;
import com.shika.football.repository.LeagueTeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/leagueTeams")
public class LeagueTeamController {

    @Autowired
    LeagueTeamRepository leagueTeamRepository;

    @GetMapping("")
    public Iterable<LeagueTeam> getAllLeaguTeams() {
        return leagueTeamRepository.findAll ();
    }
}
