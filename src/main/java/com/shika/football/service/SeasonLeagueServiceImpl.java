package com.shika.football.service;

import com.shika.football.NotFoundException;
import com.shika.football.model.League;
import com.shika.football.model.Season;
import com.shika.football.model.SeasonLeague;
import com.shika.football.repository.SeasonLeagueRepository;
import org.springframework.stereotype.Service;

@Service
public class SeasonLeagueServiceImpl implements SeasonLeagueService {

    private final SeasonLeagueRepository seasonLeagueRepository;

    public SeasonLeagueServiceImpl(SeasonLeagueRepository seasonLeagueRepository) {
        this.seasonLeagueRepository = seasonLeagueRepository;
    }

    public SeasonLeague findBySeasonAndLeague(Season season, League league) {
        return seasonLeagueRepository.findBySeasonIdAndLeagueId(season.getId(), league.getId()).orElse(null);
    }

    public SeasonLeague findBySeasonLeagueId(Long seasonLeagueId) {
        return seasonLeagueRepository.findById(seasonLeagueId).orElseThrow(() -> new NotFoundException("seasonLeague not found"));
    }


}
