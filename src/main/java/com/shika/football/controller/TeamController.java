package com.shika.football.controller;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.shika.football.model.Team;
import com.shika.football.model.Views;
import com.shika.football.repository.TeamRepository;
import com.shika.football.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/teams")
public class TeamController {

//    @Autowired
//    TeamRepository teamRepository;

    @Autowired
    TeamService teamService;

    @GetMapping("")
    @JsonView(Views.ListView.class)
    public Iterable<Team> getAllTeams() {
        return teamService.findAll();
    }

    @GetMapping("/{teamId}")
    @JsonView(Views.ListView.class)
    public Team getTeamById(@PathVariable("teamId") Long teamId) {
        return teamService.getTeamById(teamId);
    }

    @PostMapping("")
    public List<Team> bulkSaveTeams(@RequestBody() List<String> teamList) {
        return teamService.bulkSaveTeams(teamList);
    }
}
