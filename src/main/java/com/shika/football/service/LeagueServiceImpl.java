package com.shika.football.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.shika.football.NotFoundException;
import com.shika.football.model.League;
import com.shika.football.model.LeagueTeam;
import com.shika.football.model.Team;
import com.shika.football.repository.LeagueRepository;
import com.shika.football.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeagueServiceImpl implements LeagueService {
    private final
    LeagueRepository leagueRepository;

    private final
    LeagueTeamService leagueTeamService;

    @Autowired
    TeamRepository teamRepository;

    public LeagueServiceImpl(LeagueRepository leagueRepository, LeagueTeamService leagueTeamService) {
        this.leagueRepository = leagueRepository;
        this.leagueTeamService = leagueTeamService;
    }


    public League getById(Long id) {
        return  leagueRepository.findById(id).orElse(null);
    }

    public List<Team> getTeamsByLeagueId(Long id) {
        List<LeagueTeam> leagueTeams = leagueTeamService.findAllByLeagueId(id);
        return leagueTeams.stream().map(LeagueTeam::getTeam).collect(Collectors.toList());
    }

    public List<Team> saveLeagueTeams(Long leagueId, List<String> teams) {
        League league = leagueRepository.findById(leagueId).orElseThrow(()-> new NotFoundException("league"));

        List<LeagueTeam> leagueTeams = new ArrayList<>();

        for (String name: teams) {
            Team foundTeam = teamRepository.findByName(name.toLowerCase()).orElse(null);

            if ( foundTeam == null) {
                continue;
            }

            LeagueTeam existing = leagueTeamService.findByTeam(foundTeam).orElse(null);

            if (existing != null) {
                leagueTeams.add(existing);
                continue;
            }

            LeagueTeam newLeagueTeam = new LeagueTeam(league, foundTeam);
            leagueTeamService.save(newLeagueTeam);
            leagueTeams.add(newLeagueTeam);

        }
        return leagueTeams.stream().map(LeagueTeam::getTeam).collect(Collectors.toList());
    }
}
