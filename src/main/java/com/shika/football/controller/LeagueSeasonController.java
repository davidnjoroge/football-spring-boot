package com.shika.football.controller;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.shika.football.model.Fixture;
import com.shika.football.model.SeasonLeague;
import com.shika.football.model.Views;
import com.shika.football.repository.SeasonLeagueRepository;
import com.shika.football.service.FixtureService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/seasonLeagues")
public class LeagueSeasonController {

    private final
    SeasonLeagueRepository leagueSeasonRepository;

    private final
    FixtureService fixtureService;

    public LeagueSeasonController(SeasonLeagueRepository leagueSeasonRepository, FixtureService fixtureService) {
        this.leagueSeasonRepository = leagueSeasonRepository;
        this.fixtureService = fixtureService;
    }


    @GetMapping("")
    public Iterable<SeasonLeague> getAllLeagueSeasons() {
        return leagueSeasonRepository.findAll ();
    }

    @JsonView(Views.All.class)
    @GetMapping("/{seasonLeagueId}/matchWeek/{matchWeek}")
    public List<Fixture> fetchWeekFixturesForSeasonLeague(@PathVariable Long seasonLeagueId, @PathVariable Long matchWeek) {
        return fixtureService.getWeekFixturesForSeasonLeague(seasonLeagueId, matchWeek);
    }

}
