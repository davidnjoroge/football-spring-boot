package com.shika.football.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.shika.football.model.Fixture;
import com.shika.football.model.League;
import com.shika.football.model.Views;

public class SeasonLeagueRequest {
    public SeasonLeagueRequest(League league, List<Fixture> fixtures) {
        this.league = league;
        this.fixtures = fixtures;
    }

    @JsonView({Views.All.class, Views.SeasonDetailView.class})
    private League league;

    @JsonView({Views.All.class, Views.SeasonDetailView.class})
    private List<Fixture> fixtures;

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public List<Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<Fixture> fixtures) {
        this.fixtures = fixtures;
    }
}
