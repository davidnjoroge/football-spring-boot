package com.shika.football.service;

import java.util.List;

import com.shika.football.model.Fixture;
import com.shika.football.model.SeasonLeague;
import com.shika.football.model.Team;
import com.shika.football.request.FixtureRequest;

public interface FixtureService {

    List<Fixture> findBySeasonAndLeague(Long seasonId, Long leagueId);

    Fixture save(Fixture fixture);

    Fixture createFixture(SeasonLeague seasonLeague, FixtureRequest fixtureRequest);

    Fixture getOrCreateFixture(SeasonLeague seasonLeague, FixtureRequest fixtureRequest);

    Fixture findBySeasonLeagueAndHomeAndAway(SeasonLeague seasonLeague, Team home, Team away);

    List<Fixture> getWeekFixturesForSeasonLeague(Long seasonLeagueId, Long week);

    }
