package com.shika.football.model;

public interface Views {
    interface All {
    }

    interface DetailsView extends All {

    }

    interface LeagueListView extends All {
    }

    interface LeagueDetailsView extends LeagueListView {
    }

    interface ListView extends All {

    }

    interface SeasonDetailView extends All {

    }
}

