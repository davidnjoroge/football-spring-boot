package com.shika.football.service;

import java.util.List;
import java.util.Optional;

import com.shika.football.model.LeagueTeam;
import com.shika.football.model.Team;
import com.shika.football.repository.LeagueTeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeagueTeamServiceImpl implements LeagueTeamService {
    @Autowired
    LeagueTeamRepository leagueTeamRepository;
    @Override
    public List<LeagueTeam> findAllByLeagueId(Long leagueId) {
        return leagueTeamRepository.findByLeagueId(leagueId);
    }

    public LeagueTeam save(LeagueTeam leagueTeam) {
        return leagueTeamRepository.save(leagueTeam);
    }

    public Optional<LeagueTeam> findByTeam(Team team) {
        return leagueTeamRepository.findByTeamId(team.getId());
    }
}
