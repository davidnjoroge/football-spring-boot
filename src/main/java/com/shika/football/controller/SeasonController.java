package com.shika.football.controller;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.shika.football.model.Fixture;
import com.shika.football.model.League;
import com.shika.football.model.Season;
import com.shika.football.model.SeasonLeague;
import com.shika.football.model.Views;
import com.shika.football.repository.SeasonRepository;
import com.shika.football.request.FixtureRequest;
import com.shika.football.request.SeasonLeagueRequest;
import com.shika.football.service.FixtureService;
import com.shika.football.service.LeagueService;
import com.shika.football.service.SeasonLeagueService;
import com.shika.football.service.SeasonService;
import com.shika.football.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/seasons")
public class SeasonController {

    @Autowired
    SeasonRepository seasonRepository;

    @Autowired
    SeasonService seasonService;

    @Autowired
    LeagueService leagueService;

    @Autowired
    FixtureService fixtureService;

    @Autowired
    TeamService teamService;

    @Autowired
    SeasonLeagueService seasonLeagueService;


    @GetMapping("")
//    @JsonView(Views.SeasonListView.class)
    public Iterable<Season> getAllSeasons() {
        return seasonRepository.findAll();
    }

    @GetMapping("/{seasonId}")
    @JsonView(Views.DetailsView.class)
    public Season getBySeasonId(@PathVariable Long seasonId) {
        return seasonService.findBySeasonId(seasonId);

    }

    @JsonView(Views.SeasonDetailView.class)
    @GetMapping("/{seasonId}/league/{leagueId}")
    public SeasonLeagueRequest getBySeason(@PathVariable Long seasonId, @PathVariable Long leagueId) {
        Season season = seasonService.findBySeasonId(seasonId);
        League league = leagueService.getById(leagueId);

        List<Fixture> fixtures =  fixtureService.findBySeasonAndLeague(season.getId(), league.getId());
        return new SeasonLeagueRequest(league, fixtures);
    }

    @JsonView(Views.SeasonDetailView.class)
    @PostMapping("/{seasonId}/league/{leagueId}")
    public Fixture saveFixture(@PathVariable Long seasonId, @PathVariable Long leagueId, @RequestBody() FixtureRequest fixtureRequest) {
        Season season = seasonService.findBySeasonId(seasonId);
        League league = leagueService.getById(leagueId);

        SeasonLeague seasonLeague = seasonLeagueService.findBySeasonAndLeague(season, league);

        return fixtureService.getOrCreateFixture(seasonLeague, fixtureRequest);
    }

    @JsonView(Views.SeasonDetailView.class)
    @PostMapping("/{seasonId}/league/{leagueId}/bulk")
    public List<Fixture> saveBulkFixture(@PathVariable Long seasonId, @PathVariable Long leagueId, @RequestBody() List<FixtureRequest> fixtureRequests) {
        Season season = seasonService.findBySeasonId(seasonId);
        League league = leagueService.getById(leagueId);

        SeasonLeague seasonLeague = seasonLeagueService.findBySeasonAndLeague(season, league);

        List<Fixture> savedFixtures = new ArrayList<>();

        for (FixtureRequest fixtureRequest: fixtureRequests) {
            savedFixtures.add(fixtureService.getOrCreateFixture(seasonLeague, fixtureRequest));
        }

        return savedFixtures;
    }
}
