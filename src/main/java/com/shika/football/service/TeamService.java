package com.shika.football.service;

import java.util.List;
import java.util.Optional;

import com.shika.football.model.Team;

public interface TeamService {

    Team getTeamById(Long teamId);

    List<Team> findAll();

    Optional<Team> findByName(String name);

    List<Team> bulkSaveTeams(List<String> teamNames);
}
