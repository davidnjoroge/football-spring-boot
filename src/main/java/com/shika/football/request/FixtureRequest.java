package com.shika.football.request;

import java.time.LocalDate;

public class FixtureRequest {

    private String home;
    private String away;
    private LocalDate fixtureDate;
    private String fixtureTime;
    private Long matchWeek;
    private Long homeScore;
    private Long awayScore;

    public FixtureRequest(String home, String away, LocalDate fixtureDate, String fixtureTime, Long matchWeek, Long homeScore, Long awayScore) {
        this.home = home;
        this.away = away;
        this.fixtureDate = fixtureDate;
        this.fixtureTime = fixtureTime;
        this.matchWeek = matchWeek;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getAway() {
        return away;
    }

    public void setAway(String away) {
        this.away = away;
    }

    public LocalDate getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(LocalDate fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public String getFixtureTime() {
        return fixtureTime;
    }

    public void setFixtureTime(String fixtureTime) {
        this.fixtureTime = fixtureTime;
    }

    public Long getMatchWeek() {
        return matchWeek;
    }

    public void setMatchWeek(Long matchWeek) {
        this.matchWeek = matchWeek;
    }

    public Long getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Long homeScore) {
        this.homeScore = homeScore;
    }

    public Long getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Long awayScore) {
        this.awayScore = awayScore;
    }
}
